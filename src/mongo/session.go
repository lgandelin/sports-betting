package mongo

import (
	. "gitlab.com/lgandelin/utils"
	"gopkg.in/mgo.v2"
	"os"
	"time"
)

func NewMongoDatabase() (*mgo.Session, *mgo.Database) {
	session, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{os.Getenv("SPORTS_BETTING_MONGO_1_PORT_27017_TCP_ADDR")},
		Timeout:  60 * time.Second,
		Username: os.Getenv("MONGO_ENV_MONGO_INITDB_ROOT_USERNAME"),
		Password: os.Getenv("MONGO_ENV_MONGO_INITDB_ROOT_PASSWORD"),
	})
	Check(err)

	return session, session.DB("ps3838")
}

func NewBetsCollection(database *mgo.Database) *mgo.Collection {
	return database.C("bets")
}

func NewLeaguesCollection(database *mgo.Database) *mgo.Collection {
	return database.C("leagues")
}
