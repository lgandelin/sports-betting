package common

type League struct {
	ID       string `json:"id" bson:"_id"`
	LeagueID int    `json:"leagueId" bson:"leagueId"`
	Name     string `json:"name" bson:"name"`
}
