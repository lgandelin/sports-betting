package common

import (
	"time"
)

type BetStatus int

const (
	PENDING  BetStatus = 1
	PLACED   BetStatus = 2
	FINISHED BetStatus = 3
)

type Bet struct {
	ID         string    `json:"id" bson:"_id"`
	SportID    int       `json:"sportId" bson:"sportId"`
	LeagueID   int       `json:"leagueId" bson:"leagueId"`
	LeagueName string    `json:"leagueName" bson:"leagueName"`
	EventID    int       `json:"eventId" bson:"eventId"`
	EventDate  time.Time `json:"eventDate" bson:"eventDate"`
	Home       string    `json:"home" bson:"home"`
	Away       string    `json:"away" bson:"away"`
	LastPrice  float64   `json:"lastPrice" bson:"lastPrice"`
	StopPrice  float64   `json:"stopPrice" bson:"stopPrice"`
	Stake      float64   `json:"stake" bson:"stake"`
	Strategy   string    `json:"strategy" bson:"strategy"`
	Type       string    `json:"type" bson:"type"`
	Team       string    `json:"team" bson:"team"`
	Side       string    `json:"side" bson:"side"`
	Handicap   float64   `json:"handicap" bson:"handicap"`
	Status     BetStatus `json:"status" bson:"status"`
}
