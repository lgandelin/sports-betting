package main

import (
	"github.com/davecgh/go-spew/spew"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/mongo"
)

func main() {
	session, database := NewMongoDatabase()
	collection := NewBetsCollection(database)
	defer session.Close()

	var bets []Bet
	collection.Find(nil).All(&bets)
	spew.Dump(bets)
}
