package main

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	. "gitlab.com/lgandelin/api"
	"log"
	"net/http"
)

func main() {
	var router = mux.NewRouter()

	router.HandleFunc("/bets", ListBets).Methods("GET")
	router.HandleFunc("/bet", CreateBet).Methods("POST")
	router.HandleFunc("/delete_bet", DeleteBet).Methods("POST")
	router.HandleFunc("/leagues", ListLeagues).Methods("GET")
	router.HandleFunc("/events/{leagueId}", ListEvents).Methods("GET")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	handlers := c.Handler(router)

	log.Fatal(http.ListenAndServe(":3000", handlers))
}
