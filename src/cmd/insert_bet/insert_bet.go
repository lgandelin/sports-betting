package main

import (
	"bufio"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/mongo"
	. "gitlab.com/lgandelin/ps3838"
	. "gitlab.com/lgandelin/utils"
	"gopkg.in/mgo.v2/bson"
	"os"
	"time"
)

func main() {
	leagueID, eventID, eventDate, home, away, betType, team, side, handicap, stopPrice, strategy, stake := AskForArguments()

	session, database := NewMongoDatabase()
	defer session.Close()

	//Find additional informations
	leagues_collection := NewLeaguesCollection(database)

	var league League
	leagues_collection.Find(bson.M{"leagueId": leagueID}).One(&league)

	bet := Bet{
		ID:         uuid.New().String(),
		SportID:    SOCCER_SPORT_ID,
		LeagueID:   leagueID,
		LeagueName: league.Name,
		EventID:    eventID,
		EventDate:  eventDate,
		Home:       home,
		Away:       away,
		Stake:      stake,
		StopPrice:  stopPrice,
		Strategy:   strategy,
		Type:       betType,
		Team:       team,
		Side:       side,
		Handicap:   handicap,
		Status:     PENDING,
	}

	bets_collection := NewBetsCollection(database)

	err := bets_collection.Insert(bet)
	Check(err)

	fmt.Println("Inserted bet successfully")
	spew.Dump(bet)
}

func AskForArguments() (int, int, time.Time, string, string, string, string, string, float64, float64, string, float64) {
	var leagueID, eventID int
	var date, home, away, betType, team, side, strategy string
	var handicap, stopPrice, stake float64

	fmt.Print("Enter LeagueID : ")
	fmt.Scanln(&leagueID)
	fmt.Println("LeagueID entered : ", leagueID)

	fmt.Print("Enter EventID : ")
	fmt.Scanln(&eventID)
	fmt.Println("EventID entered : ", eventID)

	fmt.Print("Enter EventDate : ")
	fmt.Scanln(&date)
	eventDate, _ := time.Parse(time.RFC3339, date)
	fmt.Println("EventDate entered : ", date)

	fmt.Print("Enter Home team : ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	home = scanner.Text()
	fmt.Println("Home team entered : ", home)

	fmt.Print("Enter Away team : ")
	scanner = bufio.NewScanner(os.Stdin)
	scanner.Scan()
	away = scanner.Text()
	fmt.Println("Away team entered : ", away)

	fmt.Print("Enter Type [MONEYLINE / TOTAL_POINTS] : ")
	fmt.Scanln(&betType)
	fmt.Println("Type entered : ", betType)

	if betType == "MONEYLINE" {
		fmt.Print("Enter Team [TEAM1 / DRAW / TEAM2] : ")
		fmt.Scanln(&team)
		fmt.Println("Team entered : ", team)
	} else {
		fmt.Print("Enter Side [OVER / UNDER] : ")
		fmt.Scanln(&side)
		fmt.Println("Side entered : ", side)

		fmt.Print("Enter Handicap : ")
		fmt.Scanln(&handicap)
		fmt.Println("Handicap entered : ", handicap)
	}

	fmt.Print("Enter StopPrice : ")
	fmt.Scanln(&stopPrice)
	fmt.Println("StopPrice entered : ", stopPrice)

	fmt.Print("Enter Strategy : ")
	fmt.Scanln(&strategy)
	fmt.Println("Strategy entered : ", strategy)

	fmt.Print("Enter Stake : ")
	fmt.Scanln(&stake)
	fmt.Println("Stake entered : ", stake)

	return leagueID, eventID, eventDate, home, away, betType, team, side, handicap, stopPrice, strategy, stake
}
