package main

import (
	"fmt"
	"github.com/google/uuid"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/mongo"
	. "gitlab.com/lgandelin/ps3838"
	. "gitlab.com/lgandelin/utils"
	"os"
)

func main() {
	session, database := NewMongoDatabase()
	collection := NewLeaguesCollection(database)
	defer session.Close()

	client := NewPS3838Client(os.Getenv("PS3838_USERNAME"), os.Getenv("PS3838_PASSWORD"))

	i := 0
	for _, ps3838_league := range client.GetLeagues(SOCCER_SPORT_ID) {
		league := League{
			ID:       uuid.New().String(),
			LeagueID: ps3838_league.LeagueID,
			Name:     ps3838_league.Name,
		}

		err := collection.Insert(league)
		Check(err)

		i++
	}

	fmt.Printf("%d leagues inserted !", i)
}
