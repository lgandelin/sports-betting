package main

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/mongo"
	. "gitlab.com/lgandelin/ps3838"
	. "gitlab.com/lgandelin/utils"
	"gopkg.in/mgo.v2/bson"
	"os"
	"time"
)

func main() {
	session, database := NewMongoDatabase()
	collection := NewBetsCollection(database)
	defer session.Close()

	var bets []Bet
	collection.Find(bson.M{"status": PENDING}).All(&bets)

	client := NewPS3838Client(os.Getenv("PS3838_USERNAME"), os.Getenv("PS3838_PASSWORD"))

	BETSLOOP:
	for _, bet := range bets {
		time.Sleep(1 * time.Second)

		jsonLine := client.GetLine(bet)

		//Update last price
		lastPriceUpdate := bson.M{"$set": bson.M{"lastPrice": jsonLine.Price}}
		err := collection.Update(bson.M{"_id": bet.ID}, lastPriceUpdate)
		Check(err)

		//We take the bet 3 minutes before kick-off
		limitDate := bet.EventDate.Add(-time.Minute * time.Duration(3))

		fmt.Println("Current price for bet ", bet.EventID, ":", jsonLine.Price, "/ Stop loss : ", bet.StopPrice)

		//If we reached the limit date
		if time.Now().After(limitDate) {

			//We check that we haven't already placed the bet
			fromDate := time.Now().UTC().AddDate(0, 0, -7)
			toDate := time.Now().UTC()

			bets := client.GetCurrentBets(fromDate, toDate)

			for _, currentBet := range bets {
				//If we already have placed the bet, we stop
				if bet.EventID == currentBet.EventID && bet.Type == currentBet.BetType && bet.Team == currentBet.TeamName && bet.Side == currentBet.Side && bet.Handicap == currentBet.Handicap {
					fmt.Println("Already placed this bet. Not taking the bet")
					continue BETSLOOP
				}
			}

			//If we are in dev mode
			if os.Getenv("ENVIRONMENT") == "dev" {
				fmt.Println("Dev environment detected. Not taking the bet")
				return
			}

			//We check that the current price is greater than the stop price
			//if jsonLine.Price >= bet.StopPrice {
				response := client.PlaceBet(bet, jsonLine)
				spew.Dump(response)

				if response.Status == "ACCEPTED" {
					fmt.Println("Successfully placed the bet !")

					//We update the bet status
					statusUpdate := bson.M{"$set": bson.M{"status": PLACED}}
					err := collection.Update(bson.M{"_id": bet.ID}, statusUpdate)
					Check(err)
				}
			/*} else {
				fmt.Println("Current price below limit price. Not taking the bet")
			}*/
		} else {
			fmt.Println("Stop loss or limit date not reached for bet :", bet.EventID)
		}
	}
}
