package api

import (
	"time"
)

type CreateBetRequest struct {
	LeagueID  int       `json:"league_id"`
	EventID   int       `json:"event_id"`
	EventDate time.Time `json:"event_date"`
	Home      string    `json:"home"`
	Away      string    `json:"away"`
	Type      string    `json:"type"`
	Team      string    `json:"team"`
	Side      string    `json:"side"`
	Handicap  float64   `json:"handicap"`
	Strategy  string    `json:"strategy"`
	StopPrice float64   `json:"stop_price"`
	Stake     float64   `json:"stake"`
}

type DeleteBetRequest struct {
	BetID string `json:"bet_id"`
}
