package api

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/mongo"
	. "gitlab.com/lgandelin/ps3838"
	. "gitlab.com/lgandelin/utils"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"os"
	"strconv"
)

func ListBets(w http.ResponseWriter, r *http.Request) {
	session, database := NewMongoDatabase()
	collection := NewBetsCollection(database)
	defer session.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var bets []Bet
	collection.Find(nil).Sort("eventDate").All(&bets)

	json.NewEncoder(w).Encode(bets)
}

func ListLeagues(w http.ResponseWriter, r *http.Request) {
	session, database := NewMongoDatabase()
	collection := NewLeaguesCollection(database)
	defer session.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var leagues []League
	collection.Find(nil).Sort("name").All(&leagues)

	json.NewEncoder(w).Encode(leagues)
}

func ListEvents(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	leagueID, err := strconv.Atoi(vars["leagueId"])
	Check(err)

	client := NewPS3838Client(os.Getenv("PS3838_USERNAME"), os.Getenv("PS3838_PASSWORD"))

	//Fetch next league events
	events := client.GetEvents(SOCCER_SPORT_ID, leagueID)

	json.NewEncoder(w).Encode(events)
}

func CreateBet(w http.ResponseWriter, r *http.Request) {
	var createBetRequest CreateBetRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&createBetRequest)
	Check(err)
	defer r.Body.Close()

	session, database := NewMongoDatabase()
	bets_collection := NewBetsCollection(database)
	leagues_collection := NewLeaguesCollection(database)
	defer session.Close()

	var league League
	leagues_collection.Find(bson.M{"leagueId": createBetRequest.LeagueID}).One(&league)

	bet := Bet{
		ID:         uuid.New().String(),
		SportID:    SOCCER_SPORT_ID,
		LeagueID:   createBetRequest.LeagueID,
		LeagueName: league.Name,
		EventID:    createBetRequest.EventID,
		EventDate:  createBetRequest.EventDate,
		Home:       createBetRequest.Home,
		Away:       createBetRequest.Away,
		Stake:      createBetRequest.Stake,
		StopPrice:  createBetRequest.StopPrice,
		Strategy:   createBetRequest.Strategy,
		Type:       createBetRequest.Type,
		Team:       createBetRequest.Team,
		Side:       createBetRequest.Side,
		Handicap:   createBetRequest.Handicap,
		Status:     PENDING,
	}

	err = bets_collection.Insert(bet)
	Check(err)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func DeleteBet(w http.ResponseWriter, r *http.Request) {
	var deleteBetRequest DeleteBetRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&deleteBetRequest)
	Check(err)
	defer r.Body.Close()

	session, database := NewMongoDatabase()
	bets_collection := NewBetsCollection(database)
	defer session.Close()

	bets_collection.Remove(bson.M{"_id": deleteBetRequest.BetID})

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}
