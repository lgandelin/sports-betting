package ps3838

import (
	"time"
)

type JSONLineResponse struct {
	Status    string  `json:"status" bson:"status"`
	Price     float64 `json:"price"`
	LineID    int     `json:"lineId" bson:"lineId"`
	AltLineID int     `json:"altLineId"`
}

type JSONPlaceBetRequest struct {
	UniqueRequestId  string  `json:"uniqueRequestId"`
	AcceptBetterLine bool    `json:"acceptBetterLine"`
	Stake            float64 `json:"stake"`
	WinRiskStake     string  `json:"winRiskStake"`
	SportID          int     `json:"sportId"`
	EventID          int     `json:"eventId"`
	LineID           int     `json:"lineId"`
	AltLineID        int     `json:"altLineId"`
	PeriodNumber     int     `json:"periodNumber"`
	BetType          string  `json:"betType"`
	Team             string  `json:"team"`
	Side             string  `json:"side"`
	Handicap         float64 `json:"handicap"`
	OddsFormat       string  `json:"oddsFormat"`
}

type JSONPlaceBetResponse struct {
	Status                string  `json:"status"`
	ErrorCode             string  `json:"errorCode"`
	BetID                 int     `json:"betId"`
	UniqueRequestId       string  `json:"uniqueRequestId"`
	BetterLineWasAccepted bool    `json:"betterLineWasAccepted"`
	Price                 float64 `json:"price"`
}

type JSONBetsResponse struct {
	Bets []JSONBet `json:"bets"`
}

type JSONBet struct {
	BetID       int       `json:"betId"`
	WagerNumber int       `json:"wagerNumber"`
	PlacedAt    time.Time `json:"placedAt"`
	BetStatus   string    `json:"betStatus"`
	BetType     string    `json:"betType"`
	Win         float64   `json:"win"`
	Risk        float64   `json:"risk"`
	WinLoss     float64   `json:"winLoss"`
	OddsFormat  string    `json:"oddsFormat"`
	SportID     int       `json:"sportId"`
	LeagueID    int       `json:"leagueId"`
	EventID     int       `json:"eventId"`
	Price       float64   `json:"price"`
	TeamName    string    `json:"teamName"`
	Side        string    `json:"side"`
	Handicap    float64   `json:"handicap"`
}

type JSONLeaguesResponse struct {
	Leagues []JSONLeague `json:"leagues"`
}

type JSONLeague struct {
	LeagueID int    `json:"id"`
	Name     string `json:"name"`
}

type JSONFixturesResponse struct {
	SportID int                  `json:"sportId"`
	Leagues []JSONFixturesLeague `json:"league"`
}

type JSONFixturesLeague struct {
	ID     int         `json:"id"`
	Events []JSONEvent `json:"events"`
}

type JSONEvent struct {
	EventID   int       `json:"id"`
	EventDate time.Time `json:"starts"`
	Home      string    `json:"home"`
	Away      string    `json:"away"`
	Status    string    `json:"status"`
}
