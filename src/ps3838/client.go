package ps3838

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/utils"
	"io/ioutil"
	"net/http"
	"time"
)

type PS3838Client struct {
	BaseURL    string
	HttpClient *http.Client
	Token      string
}

func NewPS3838Client(username, password string) *PS3838Client {
	credentials := username + ":" + password
	token := base64.StdEncoding.EncodeToString([]byte(credentials))

	return &PS3838Client{
		BaseURL:    "http://api.ps3838.com",
		HttpClient: &http.Client{},
		Token:      token,
	}
}

func (client *PS3838Client) GetLeagues(sportID int) []JSONLeague {
	endpoint := client.BaseURL + fmt.Sprintf("/v2/leagues?sportId=%d", sportID)
	request, err := http.NewRequest("GET", endpoint, nil)
	request.Header.Add("Authorization", "Basic "+client.Token)
	response, err := client.HttpClient.Do(request)
	Check(err)

	body, err := ioutil.ReadAll(response.Body)
	Check(err)

	leagues := JSONLeaguesResponse{}
	err = json.Unmarshal(body, &leagues)
	Check(err)

	return leagues.Leagues
}

func (client *PS3838Client) GetEvents(sportID int, leagueID int) []JSONEvent {
	endpoint := client.BaseURL + fmt.Sprintf("/v1/fixtures?sportId=%d&leagueids=%d", sportID, leagueID)
	request, err := http.NewRequest("GET", endpoint, nil)
	request.Header.Add("Authorization", "Basic "+client.Token)
	response, err := client.HttpClient.Do(request)
	Check(err)

	body, err := ioutil.ReadAll(response.Body)
	Check(err)

	fixtures := JSONFixturesResponse{}

	if len(body) > 0 {
		err = json.Unmarshal(body, &fixtures)
		Check(err)

		return fixtures.Leagues[0].Events
	}

	return []JSONEvent{}
}

func (client *PS3838Client) GetLine(bet Bet) JSONLineResponse {
	//We fetch the last bet infos
	endpoint := client.BaseURL + fmt.Sprintf("/v1/line?sportid=%d&leagueid=%d&eventid=%d&periodNumber=0&betType=%s&oddsFormat=DECIMAL", bet.SportID, bet.LeagueID, bet.EventID, bet.Type)

	if bet.Type == "MONEYLINE" {
		endpoint += fmt.Sprintf("&team=%s", bet.Team)
	} else if bet.Type == "TOTAL_POINTS" {
		endpoint += fmt.Sprintf("&side=%s&handicap=%.2f", bet.Side, bet.Handicap)
	}

	request, err := http.NewRequest("GET", endpoint, nil)
	request.Header.Add("Authorization", "Basic "+client.Token)

	response, err := client.HttpClient.Do(request)
	Check(err)

	body, err := ioutil.ReadAll(response.Body)
	Check(err)

	jsonLine := JSONLineResponse{}
	err = json.Unmarshal(body, &jsonLine)
	Check(err)

	return jsonLine
}

func (client *PS3838Client) GetCurrentBets(fromDate, toDate time.Time) []JSONBet {
	endpoint := client.BaseURL + fmt.Sprintf("/v1/bets?betlist=RUNNING&fromDate=%s&toDate=%s", fromDate.Format(time.RFC3339), toDate.Format(time.RFC3339))
	request, err := http.NewRequest("GET", endpoint, nil)
	request.Header.Add("Authorization", "Basic "+client.Token)
	response, err := client.HttpClient.Do(request)
	Check(err)

	body, err := ioutil.ReadAll(response.Body)
	Check(err)

	bets := JSONBetsResponse{}
	err = json.Unmarshal(body, &bets)
	Check(err)

	return bets.Bets
}

func (client *PS3838Client) PlaceBet(bet Bet, jsonLine JSONLineResponse) JSONPlaceBetResponse {
	data := JSONPlaceBetRequest{
		UniqueRequestId:  uuid.New().String(),
		AcceptBetterLine: true,
		Stake:            bet.Stake,
		WinRiskStake:     "RISK",
		LineID:           jsonLine.LineID,
		AltLineID:        jsonLine.AltLineID,
		SportID:          bet.SportID,
		EventID:          bet.EventID,
		PeriodNumber:     0,
		BetType:          bet.Type,
		Team:             bet.Team,
		Side:             bet.Side,
		Handicap:         bet.Handicap,
		OddsFormat:       "DECIMAL",
	}

	jsonData, err := json.Marshal(data)
	Check(err)

	endpoint := client.BaseURL + fmt.Sprintf("/v1/bets/place")
	request, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(jsonData))
	request.Header.Add("Authorization", "Basic "+client.Token)
	request.Header.Set("Content-Type", "application/json")
	Check(err)

	response, err := client.HttpClient.Do(request)
	Check(err)

	body, err := ioutil.ReadAll(response.Body)
	Check(err)

	bet_response := JSONPlaceBetResponse{}
	err = json.Unmarshal(body, &bet_response)
	Check(err)

	return bet_response
}
